const jsml = require('jsml-davidystephenson')

const is = value => value
const isString = value => typeof value === 'string'
const isNumber = value => !isNaN(value) && typeof value === 'number'
const isAnyObject = value => value === Object(value)
// const isValid = value => isString(value) || isNumber(value) || isAnyObject(value)

const isArray = value => Array.isArray(value)
const isFunction = value => value && {}.toString.call(value) === '[object Function]'
const isObject = value => !Array.isArray(value) &&
  !isFunction(value) &&
  isAnyObject(value)

// const isElement = value => typeof Element === 'object'
//   ? value instanceof Element
//   : value &&
//     typeof value === 'object' &&
//     value !== null &&
//     value.nodeType === 1 &&
//     typeof value.nodeName === 'string'

const strip = (data, keys) => {
  const clone = Object.assign({}, data)
  const values = {}

  if (clone) {
    keys.map(key => {
      const value = clone[key]

      if (value) {
        values[key] = value
        clone[key] = null
      }
    })
  }

  return [clone, values]
}

const enrich = (element, data, enricher) => {
  if (data) {
    Object.entries(data).map(entry => {
      const key = entry[0]
      const value = entry[1]

      enricher(element, key, value)
    })
  }
}

const listen = (element, key, value) => element.addEventListener(key, value)

const renderer = window => {
  const isNode = value => typeof window.Node === 'object'
    ? value instanceof window.Node
    : value &&
      typeof value === 'object' &&
      typeof value.nodeType === 'number' &&
      typeof value.nodeName === 'string'
  const isJquery = value => window.jQuery && value instanceof window.jQuery

  const build = data => {
    const [stripped, values] = strip(data, ['child', 'on'])

    const html = jsml(stripped)

    const template = window.document.createElement('template')
    template.innerHTML = html

    const element = template.content.firstChild

    enrich(element, values.on, listen)
    append(element, values.child)

    return element
  }

  const append = (a, b) => {
    if (a && b) {
      if (isJquery(a) || isNode(a)) {
        if (isJquery(b)) {
          b.each((index, element) => a.append(element))
        } else if (isNode(b)) {
          a.append(b)
        } else {
          const c = make(b)

          append(a, c)
        }
      } else {
        const d = make(a)

        append(d, b)
      }
    }
  }

  const make = data => {
    if (isNode(data) || isJquery(data)) {
      return data
    } else if (isArray(data)) {
      const fragment = window.document.createDocumentFragment()

      data
        .map(make)
        .map(node => append(fragment, node))

      return fragment
    } else if (isFunction(data)) {
      return make(data())
    } else if (isObject(data)) {
      return build(data)
    } else if (isString(data)) {
      return window.document.createTextNode(data)
    } else if (isNumber(data)) {
      return make(data.toString())
    } else {
      return null
    }
  }

  return (data, ...children) => {
    const base = make(data)

    children
      .map(make)
      .filter(is)
      .map(child => append(base, child))

    return base
  }
}

if (typeof window !== 'undefined' && typeof window.document !== 'undefined') {
  window.jsml = renderer(window)
}

module.exports = renderer
