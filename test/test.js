/* global describe, it, beforeEach */

const assert = require('assert')
const jsdom = require('jsdom')
const jsmileRenderer = require('../src/index')
const sinon = require('sinon')

  // TODO Express cache
  // TODO Add standard module as dependency
  // TODO Adopt method? adopt = (view, child) => include(view, { child })
  // TODO Render method render = view, options => build(include(view, options))

;(() => {
  describe('render', () => {
    let dom, document, jsml, $

    beforeEach(() => {
      const html = '<!DOCTYPE html><html><head></head><body></body></html>'
      dom = new jsdom.JSDOM(html)
      const { window } = dom
      document = window.document

      jsml = jsmileRenderer(window)

      global.window = window
      global.document = document
      $ = global.jQuery = require('jquery')
    })

    describe('object', () => {
      it('parses elements', () => {
        const div = jsml({})
        const element = document.createElement('div')

        assert(div.isEqualNode(element))
      })

      describe('assigns child', () => {
        it('text', () => {
          const paragraph = jsml({ tag: 'p', child: 'hello world' })

          const text = document.createTextNode('hello world')
          const element = document.createElement('p')
          element.appendChild(text)

          assert(paragraph.isEqualNode(element))
        })

        it('arrays', () => {
          const paragraph = jsml({ tag: 'p', child: [{}, 'hello world'] })
          const element = document.createElement('p')

          const div = document.createElement('div')
          element.appendChild(div)

          const text = document.createTextNode('hello world')
          element.appendChild(text)

          assert(paragraph.isEqualNode(element))
        })

        it('objects', () => {
          const heading = jsml({
            tag: 'h1',
            child: {
              tag: 'span',
              child: [
                { tag: 'a', href: '#' },
                'subtitle'
              ]
            }
          })

          const span = document.createElement('span')

          const a = document.createElement('a')
          a.setAttribute('href', '#')
          span.appendChild(a)

          const text = document.createTextNode('subtitle')
          span.appendChild(text)

          const h1 = document.createElement('h1')
          h1.appendChild(span)

          assert(heading.isEqualNode(h1))
        })
      })

      it('works with jQuery', () => {
        const element = jsml({ tag: 'p', child: 'hello world' })
        const body = $('body')
        body.append(element)
        assert(body.find('p')[0].isEqualNode(element))
      })

      it('attaches passed eventHandlers', () => {
        const clickSpy = sinon.spy()

        const data = { on: { click: clickSpy } }
        const element = jsml(data)
        element.click()

        assert.strictEqual(clickSpy.callCount, 1)
      })

      it('attaches passed data attributes', () => {
        const quote = "Don't quote me on that."
        const data = { data: { quote } }
        const element = jsml(data)

        assert.strictEqual(quote, element.dataset.quote)
      })

      it('nests child elements', () => {
        const data = {
          child: [
            'The count is ',
            5,
            {
              tag: 'button',
              type: 'submit',
              child: [
                'Do you agree?',
                {
                  tag: 'p',
                  child: {
                    tag: 'b',
                    child: 'Please be sure.'
                  }
                }
              ]
            }
          ]
        }

        const rendered = jsml(data)

        const html = '<div>The count is 5<button type="submit">Do you agree?<p><b>Please be sure.</b></p></button></div>'

        assert.strictEqual(rendered.outerHTML, html)
      })
    })

    describe('array', () => {
      it(
        'returns an fragment with rendered elements interspersed with spaces',
        () => {
          const data = ['The count is ', '1']
          const rendered = jsml(data)

          const fragment = document.createDocumentFragment()

          const nodes = [
            document.createTextNode('The count is '),
            document.createTextNode('1')
          ]
          nodes.map(node => fragment.append(node))

          assert(rendered.isEqualNode(fragment))
        }
      )

      it('renders text and objects in a single array', () => {
        const data = ['A div:', {}]
        const rendered = jsml(data)

        const fragment = document.createDocumentFragment()

        const nodes = [
          document.createTextNode('A div:'),
          document.createElement('div')
        ]
        nodes.map(node => fragment.append(node))

        assert(rendered.isEqualNode(fragment))
        assert.strictEqual(rendered.outerHTML, fragment.outerHTML)
      })
    })

    describe('node', () => {
      it('should return the node', () => {
        const node = document.createTextNode('hello world')
        const output = jsml(node)
        assert.strictEqual(output.textContent, 'hello world')
      })

      it('should return an element when passed an element', () => {
        const div = document.createElement('div')
        const text = document.createTextNode('hello world')
        div.append(text)
        const output = jsml(div)
        assert.strictEqual(output.outerHTML, '<div>hello world</div>')
      })
    })

    describe('appending', () => {
      it('appends a node inside another node', () => {
        const container = document.createElement('div')
        const content = document.createElement('span')

        jsml(container, content)

        assert.strictEqual(container.outerHTML, '<div><span></span></div>')
      })

      it('appends multiple nodes inside another node', () => {
        const container = document.createElement('p')
        const hello = document.createTextNode('hello ')
        const world = document.createTextNode('world')
        jsml(container, hello, world)
        assert.strictEqual(container.outerHTML, '<p>hello world</p>')
      })

      it('renders the base node', () => {
        const message = jsml({}, document.createTextNode('hello world'))
        assert.strictEqual(message.outerHTML, '<div>hello world</div>')
      })

      it('renders child nodes', () => {
        const div = document.createElement('div')
        jsml(div, ['hello ', { tag: 'span', child: 'world' }], ' how are you?')
        assert.strictEqual(div.outerHTML, '<div>hello <span>world</span> how are you?</div>')
      })

      it('renders base and child nodes', () => {
        const button = jsml(
          { tag: 'button' },
          { tag: 'i', class: 'icon-delete' },
          ['Please! ', { tag: 'span', child: "Don't kill me!" }]
        )

        assert.strictEqual(
          button.outerHTML,
          `<button><i class="icon-delete"></i>Please! <span>Don't kill me!</span></button>`
        )
      })
    })
  })

  describe('integration', () => {
    let window

    beforeEach(function () {
      const virtualConsole = new jsdom.VirtualConsole()
      virtualConsole.sendTo(console)

      return jsdom
        .JSDOM
        .fromFile(
          'test/test.html',
          { resources: 'usable', runScripts: 'dangerously', virtualConsole }
        )
        .then((dom) => { window = dom.window })
    })

    it('is not automatically available in node', () => {
      assert.strictEqual(global.jsml, undefined)
    })

    it('automatically provides render', done => {
      window.addEventListener('load', () => {
        const greeting = window.document.querySelector('div').textContent
        assert.strictEqual(greeting, 'hello world')

        done()
      })
    })

    it('automatically provides jsml', done => {
      window.addEventListener('load', () => {
        const exclamation = window.document.querySelector('span').textContent
        assert.strictEqual(exclamation, '!')

        done()
      })
    })

    it('appends to all elements in a jQuery collection', done => {
      window.addEventListener('load', () => {
        const items = window.document.querySelectorAll('li')

        for (const item of items) {
          assert.strictEqual(item.textContent, 'something!')
        }

        done()
      })
    })

    it('appends all elements in a jQuery collection', done => {
      window.addEventListener('load', () => {
        const list = window.document.querySelector('ul')

        assert.strictEqual(list.childNodes.length, 3)

        list.childNodes.forEach(node => {
          assert.strictEqual(node.tagName, 'LI')
          assert.strictEqual(node.textContent, 'something!')
        })

        done()
      })
    })
  })
})()
